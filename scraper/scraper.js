var fs = require("fs");
var request = require("request-promise");
var iconv = require('iconv-lite');

// https://www.kanunu8.com/book4/10665/188324.html

async function getData (base, ext) {

  try {
    var uri = encodeURI(base + ext + ".html");

    var out = await request({uri: uri, encoding: "binary"});

    var regex = new RegExp("<p>([^]*)<\/p>", "gm");

    var catches = regex.exec(out)[1];

    // Replace chars in catches
    catches = catches.replace(/&nbsp;/g, "").replace(/<br>/g, "").replace(/<br \/>/g, "");

    var buffer = Buffer.from(catches, "binary");
    var decoded = iconv.decode(buffer, "GBK");

    return decoded;
  } catch (err) {
    console.log("Error while scraping this page. Skipping.");
    return "";
  }

};

var books = [
  {link: "https://www.kanunu8.com/book4/10665/", from: 188324, to: 188347},
  {link: "https://www.kanunu8.com/book4/10664/", from: 188293, to: 188313},
  {link: "https://www.kanunu8.com/book/4514/", from: 56923, to: 56993},
  {link: "https://www.kanunu8.com/book/4517/", from: 58475, to: 176938},
  {link: "https://www.kanunu8.com/book4/10139/", from: 225275, to: 225318},
  {link: "https://www.kanunu8.com/book4/10145/", from: 225414, to: 225731}
]

async function getBooks () {
  for (var i = 0; i < books.length; i++) {

    var data = books[i];
    var text = new String();

    var addition = 0;

    for (var j = data.from; j < data.to + 1; j++) {

      var add = await getData(data.link, j.toString());

      text += add;
      console.log("At: %s. %s/%s", data.link, j - data.from, data.to - data.from);

      if (add === "") {
        addition++;

        if (addition >= 5) {
          break;
        };

      } else {
        addition = 0;
      };
    };

    fs.writeFileSync(__dirname + "/" + i.toString() + ".txt", text);

  };

};

getBooks();
