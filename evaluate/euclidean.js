var fs = require("fs");

function getEmbeddings (directory) {
  var contents = fs.readFileSync(__dirname + directory, "utf8").split("\n");

  var embeddings = new Array();

  for (var i = 0; i < contents.length; i++) {
    var string = contents[i];

    if (string === "") {
      continue;
    };

    var coordinates = string.substring(4, string.length - 2).split(",").map(x => parseFloat(x));
    var character = string.substring(0, 1);

    embeddings.push({character: character, coordinates: coordinates});

  };

  return embeddings;
};

var embeddings = getEmbeddings("/Ch5-242000.eb");

var coordinates = getCharCoordinates("女", embeddings);
//var coordinates_b = getCharCoordinates("猫", embeddings);

//var coordinates = add(coordinates_a, coordinates_b)

var lookup = euclideanLookup(coordinates, embeddings);

for (var i = 0; i < Math.min(20, lookup.length); i++) {
  console.log("%s: %s (%s)", i+1, lookup[i].character, lookup[i].distance);
};

// Lookup
function getCharCoordinates (character, embeddings) {
  return (embeddings.find(x => x.character === character) || {}).coordinates;
};

function add (a1, a2) {
  var ret = new Array();

  for (var i = 0; i < a1.length; i++) {
    ret.push(a1[i] + a2[i]);
  };

  return ret;
};

function subtract (a1, a2) {
  var ret = new Array();

  for (var i = 0; i < a1.length; i++) {
    ret.push(a1[i] - a2[i]);
  };

  return ret;
};

function euclideanLookup (coordinates, embeddings) {

  var array = Array.from(embeddings);

  // Calculate distance
  for (var i = 0; i < array.length; i++) {
    var compound = 0;

    for (var j = 0; j < coordinates.length; j++) {
      compound += Math.pow(coordinates[j] - array[i].coordinates[j], 2);
    };

    array[i].distance = Math.sqrt(compound);

  };

  array.sort(function (a, b) {
    return a.distance - b.distance;
  });

  return array;

};
