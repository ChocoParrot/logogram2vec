import auxils
import tensorflow as tf
import numpy as np

import os

import model

main_dir = os.path.dirname(os.path.abspath(__file__))

data = auxils.Data()
m = model.Model(vocabulary=data.vocabulary, adjacents=2, embedding_dims=140)

def saveModel (session, directory):
    saver = tf.train.Saver()
    saver.save(session, main_dir + "/" + directory)

def getTestSet (vocabulary, indices=False):

    assert isinstance(vocabulary, list)

    ret = list()

    for i in range(len(vocabulary)):
        if indices:
            ret.append(i)
        else:
            ret.append(data.drawChar(vocabulary[i]))

    return ret

def writeEmbeddings (vocabulary, embeddings, directory):

    assert len(vocabulary) == len(embeddings)

    file = open(main_dir + "/" + directory, "w+", encoding="utf8")

    for a, b in zip(vocabulary, embeddings):
        file.write("{}: {}\n".format(a, list(b)))

    file.close()

with tf.Session() as session:

    session.run(tf.global_variables_initializer())

    tf.summary.scalar("Loss", m.loss)
    tf.summary.scalar("Metric", m.metric)

    tf.summary.image("Fed-input", m.input_expanded, max_outputs=2)

    merged = tf.summary.merge_all()
    writer = tf.summary.FileWriter(main_dir + "/tensorboard/Ch12", session.graph)

    while True:

        _, images, expect = data.getData(amount=40, adjacents=2)
        summary, iteration, loss, metric, _ = session.run([merged, m.global_step, m.loss, m.metric, m.optimiser], feed_dict={m.input: images, m.expected: expect})

        if iteration % 2000 == 0 or iteration == 1:

            chars = getTestSet(data.vocabulary)
            output = session.run(m.embedding, feed_dict={m.input: chars})
            writeEmbeddings(data.vocabulary, output, "/embeddings_3/Ch12-" + str(iteration) + ".eb")

        if iteration % 2000 == 0:
            print("Attempting save...")
            saveModel(session, "saves/Choco-12.ckpt")
            print("Save complete")

        writer.add_summary(summary, iteration)

        print("Iteration {}; loss: {}; metric: {}".format(iteration, loss, metric))

#print(data.data_length)
#print(data.vocabulary)
