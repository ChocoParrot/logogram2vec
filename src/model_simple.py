import tensorflow as tf

class Model ():

    def __init__ (self, vocabulary, adjacents=2, embedding_dims=4):

        vocab_size = len(vocabulary)

        self.embedding_dims = embedding_dims

        self.input = tf.placeholder(shape=[None], dtype=tf.int32)
        self.expected = tf.placeholder(shape=[None, adjacents * 2], dtype=tf.int32)

        """
        Images are between the range (0-255).
        Therefore, they are normalised to fit the neural network;
        as a preventive measure against explosions in the
        gradient and subsequently, in the weights.
        """

        with tf.variable_scope("Embedding", reuse=tf.AUTO_REUSE):

            one_hot = tf.one_hot(self.input, vocab_size)
            self.embedding = tf.layers.dense(one_hot, self.embedding_dims, activation=tf.nn.leaky_relu)

        with tf.variable_scope("Outputs", reuse=tf.AUTO_REUSE):

            output, self.loss, self.metric = self.createOutputs(self.embedding, self.expected, vocab_size)
            self.output = tf.concat(output, axis=1)

        with tf.variable_scope("Optimiser", reuse=tf.AUTO_REUSE):

            """
            The global step is integrated into the optimiser here.
            This is to allow the model to keep track of the current iteration
            and update it simultaneously when training.

            Decay in the learning rate is not used;
            this is because the model is designed to map semantic
            logograms, therefore there will be a lesser need to
            fine tune the training after numerous iterations.
            """

            self.global_step = tf.Variable(0, dtype=tf.int32)
            self.optimiser = tf.train.GradientDescentOptimizer(learning_rate=0.007).minimize(self.loss, global_step=self.global_step)

    def createOutputs (self, x, expect, vocab_size):

        neighbours = int(expect.shape[1])

        words = list()
        losses = 0
        metric = 0

        for i in range(neighbours):
            logits = tf.layers.dense(x, vocab_size, activation=None)

            labels = tf.one_hot(expect[:,i], vocab_size)

            loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(labels=labels, logits=logits))

            logistics = tf.nn.softmax(logits)

            metric += tf.reduce_mean(tf.cast(tf.equal(tf.argmax(labels, axis=1), tf.argmax(logits, axis=1)), dtype=tf.float32))

            words.append(tf.expand_dims(logistics, 1))

            losses += loss

        return words, losses, (metric / neighbours) * 100
