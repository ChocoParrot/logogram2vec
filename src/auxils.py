import os
from PIL import Image, ImageDraw, ImageFont
import numpy as np
import re

data_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + "/data/"

class Data ():

    def __init__ (self):

        # Read data
        data = open(data_dir + "data.txt", "r", encoding="utf8").read()

        # Remove all non-Han characters
        filter = re.compile(u"[^\u4E00-\u9FA5]")
        self.data = filter.sub(r"", data)

        self.data_length = len(self.data)

        self.vocabulary = list(set(self.data))
        self.vocab_size = len(self.vocabulary)
        self.eyes = np.eye(self.vocab_size)

        self.index = 3

    def lookup (self, char):

        if (char in self.vocabulary):

            return self.vocabulary.index(char)

        else:
            return -1

    def drawChar (self, char):

        assert isinstance(char, str)

        W, H = (64, 64)

        image = Image.new("L", (W, H), color=255)

        draw = ImageDraw.Draw(image)

        font = ImageFont.truetype(data_dir + "arial_unicode.ttf", 45)

        w, h = draw.textsize(char, font=font)
        draw.text(((W - w)/2, 0), char, fill=0, font=font)

        #image.save(data_dir + "out.png")

        return np.array(image)

    def getData(self, amount=5, adjacents=2, get_image=True):

        def vx (index):
            return index % self.vocab_size

        images = list()
        one_hots = list()
        indices = list()

        for i in range(amount):

            # Get current character
            current_char = self.data[vx(self.index)]

            if get_image:

                image = self.drawChar(current_char)

                # Create image
                images.append(image)
            
            indices.append(self.lookup(current_char))

            current_one_hots = list()
            for j in range(adjacents * 2 + 1):
                val = (adjacents - j)

                if val == 0:
                    continue

                else:
                    one_hot = self.lookup(self.data[vx(self.index + val)])
                    current_one_hots.append(one_hot)

            one_hots.append(current_one_hots)
            self.index += 1

        return indices, images, one_hots
