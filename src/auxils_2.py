import os
from PIL import Image, ImageDraw, ImageFont
import numpy as np
import re

from hanziconv import HanziConv
import jieba

data_dir = "B:/zhwiki-dumps"

class Data ():

    def __init__ (self, dumps=1, simplified=True, segment=True, truncate=200000):

        # Read data
        alpha_dumps = os.listdir(data_dir)

        assert dumps <= len(alpha_dumps)

        data = str()

        for i in range(dumps):
            print("Reading dump #{}.".format(i))
            articles = os.listdir(data_dir + "/" + alpha_dumps[i])

            for article in articles:
                try:
                    file_path = data_dir + "/" + alpha_dumps[i] + "/" + article
                    file = open(file_path, encoding="utf8")
                    data += file.read()

                    file.close()

                except:
                    print("Skipping.")

        print("Processing characters.")
        print("Removing non-Chinese characters.")

        # Remove all non-Han characters
        filter = re.compile(u"[^\u4E00-\u9FA5]")
        data = filter.sub(r"", data)[:truncate]

        if simplified:
            print("Converting to simplified Chinese. This may take a while.")
            data = HanziConv.toSimplified(data)
        else:
            print("Converting to traditional Chinese. This may take a while.")
            data = HanziConv.toTraditonal(data)

        if segment:
            print("Segmenting Chinese characters. This may take a while.")
            self.data = list(jieba.cut(data, cut_all=False))

        else:
            self.data = data

        self.data_length = len(self.data)

        self.vocabulary = list(set(self.data))
        self.vocab_size = len(self.vocabulary)
        self.eyes = np.eye(self.vocab_size)

        print("Text processing complete.")
        print("Data length: {}, vocabulary size: {}".format(self.data_length, self.vocab_size))

        self.index = 3

    def lookup (self, char):

        if (char in self.vocabulary):

            return self.vocabulary.index(char)

        else:
            return -1

    def drawChar (self, char):

        assert isinstance(char, str)

        W, H = (64, 64)

        image = Image.new("L", (W, H), color=255)

        draw = ImageDraw.Draw(image)

        font = ImageFont.truetype(data_dir + "arial_unicode.ttf", 45)

        w, h = draw.textsize(char, font=font)
        draw.text(((W - w)/2, 0), char, fill=0, font=font)

        #image.save(data_dir + "out.png")

        return np.array(image)

    def getData(self, amount=5, adjacents=2, get_image=True):

        def vx (index):
            return index % self.vocab_size

        images = list()
        one_hots = list()
        indices = list()

        for i in range(amount):

            # Get current character
            current_char = self.data[vx(self.index)]

            if get_image:

                image = self.drawChar(current_char)

                # Create image
                images.append(image)

            indices.append(self.lookup(current_char))

            current_one_hots = list()
            for j in range(adjacents * 2 + 1):
                val = (adjacents - j)

                if val == 0:
                    continue

                else:
                    one_hot = self.lookup(self.data[vx(self.index + val)])
                    current_one_hots.append(one_hot)

            one_hots.append(current_one_hots)
            self.index += 1

        return indices, images, one_hots
