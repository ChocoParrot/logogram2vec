import tensorflow as tf

class Model ():

    def __init__ (self, vocabulary, adjacents=2, embedding_dims=4):

        vocab_size = len(vocabulary)

        self.embedding_dims = embedding_dims

        self.input = tf.placeholder(shape=[None, 64, 64], dtype=tf.float32)
        self.expected = tf.placeholder(shape=[None, adjacents * 2, vocab_size], dtype=tf.float32)

        """
        Images are between the range (0-255).
        Therefore, they are normalised to fit the neural network;
        as a preventive measure against explosions in the
        gradient and subsequently, in the weights.
        """

        input_normalised = self.input / 255
        input_expanded = tf.expand_dims(input_normalised, 3)

        # Create convolutions
        with tf.variable_scope("Convolutions", reuse=tf.AUTO_REUSE):

            """
            Subsequent convolutions are strided and attempt to reduce latent size.

            Pooling is not used with the intuition being that forced downsampling of the
            latents or the images directly is a gross generalisation of useful information
            in the logograms.
            """

            conv1_1 = tf.layers.conv2d(input_expanded, 32, 5, strides=2, padding="same", activation=tf.nn.leaky_relu)

            conv2_1 = tf.layers.conv2d(conv1_1, 32, 5, strides=2, padding="same", activation=tf.nn.leaky_relu)

            conv3_1 = tf.layers.conv2d(conv2_1, 64, 5, strides=2, padding="same", activation=tf.nn.leaky_relu)

            conv4_1 = tf.layers.conv2d(conv3_1, 80, 5, strides=2, padding="same", activation=tf.nn.leaky_relu)

        with tf.variable_scope("Deconvolutions", reuse=tf.AUTO_REUSE):

            """
            Perform deconvolutions on the convolved latents.

            Deconvolutions will attempt to regurgitate the original image.
            """

            deconv1_1 = tf.layers.conv2d_transpose(conv4_1, 80, 5, strides=2, padding="same", activation=tf.nn.leaky_relu)

            deconv2_1 = tf.layers.conv2d_transpose(deconv1_1, 64, 5, strides=2, padding="same", activation=tf.nn.leaky_relu)

            deconv3_1 = tf.layers.conv2d_transpose(deconv2_1, 32, 5, strides=2, padding="same", activation=tf.nn.leaky_relu)

            deconv4_1 = tf.layers.conv2d_transpose(deconv3_1, 1, 5, strides=2, padding="same", activation=tf.nn.sigmoid)

        with tf.variable_scope("Embedding", reuse=tf.AUTO_REUSE):

            flatten = tf.layers.flatten(conv4_1)
            self.embedding = tf.layers.dense(flatten, self.embedding_dims, activation=tf.nn.leaky_relu)

        with tf.variable_scope("Outputs", reuse=tf.AUTO_REUSE):

            self.output = tf.layers.dense()

            self.ae_loss = tf.losses.mean_pairwise_squared_error(input_expanded, deconv4_1)
            self.ae_output = deconv4_1 * 255

        with tf.variable_scope("Optimiser", reuse=tf.AUTO_REUSE):

            """
            The global step is integrated into the optimiser here.
            This is to allow the model to keep track of the current iteration
            and update it simultaneously when training.

            Decay in the learning rate is not used;
            this is because the model is designed to map semantic
            logograms, therefore there will be a lesser need to
            fine tune the training after numerous iterations.
            """

            self.global_step = tf.Variable(0, dtype=tf.int32)

            embedding_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope="embedding")
            ae_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope="deconvolutions") + tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope="convolutions")

            self.optimiser = tf.train.GradientDescentOptimizer(learning_rate=0.002).minimize(self.loss, global_step=self.global_step, var_list=embedding_vars)
            self.ae_optimiser = tf.train.AdamOptimizer(leanring_rate=0.001, beta1=0.7, beta2=0.5).minimize(self.ae_loss, global_step=self.global_step, var_list=ae_vars)

    def createOutputs (self, x, expect, vocab_size):

        neighbours = int(expect.shape[1])

        words = list()
        losses = 0

        for i in range(neighbours):
            logits = tf.layers.dense(x, vocab_size, activation=None)
            loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(labels=expect[:,i], logits=logits))

            words.append(logits)

            losses += loss

        return words, losses
